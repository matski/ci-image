FROM alpine:3.16.0

COPY gitlab-terraform.sh /usr/bin/gitlab-terraform

# gcompat libc6-compat libcap gettext idn2-utils
RUN apk add --no-cache wget git unzip openssl ansible nomad consul openssh terraform 
RUN wget --no-verbose https://releases.hashicorp.com/vault/1.12.1/vault_1.12.1_linux_amd64.zip -O /usr/local/bin/vault_1.12.1_linux_amd64.zip && \
    pwd && \
    ls -lah /usr/local/bin && \
    unzip /usr/local/bin/vault_1.12.1_linux_amd64.zip -d /usr/local/bin/ && \
    ls -lah /usr/local/bin && \
    rm -f /usr/local/bin/vault_1.12.1_linux_amd64.zip && \
    ls -lah /usr/local/bin && \
    vault version && \
    nomad version && \
    openssl version

CMD ["/usr/local/bin/nomad"]